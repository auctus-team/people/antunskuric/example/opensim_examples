from opensim import *
import numpy as np
import random 


def setCoordinateToNeutral(model: Model, state: State):
    """!
    
    Method used to set to neutral value all of the values of the articulations of the system.

    """

    coordSet = model.updCoordinateSet()

    for k in range(coordSet.getSize()):
        coord = model.updCoordinateSet().get(k)
        default = coord.getDefaultValue()
        coord.setValue(state,default,False)
    
    model.assemble(state)

def ssetCoordinateToRandom(model: Model, state: State):
    """!
    
    Method used to set to random values all of the values of the articulations of the system.

    """
    coordSet = model.updCoordinateSet()

    for coord in coordSet:
        if not coord.isConstrained(state): # update only not constrained variables
            min_r = coord.get_range(0)
            max_r = coord.get_range(1)
            rand = (max_r - min_r )*random.random() + min_r
            coord.setValue(state, rand, False)
    
    model.assemble(state)

def setCoordinateValues(model: Model, state: State, q: list, only_clamped: bool =True):
    """!
    
    Method used to set to random values all of the values of the articulations of the system.

    Args: 
        q : list
            List of joint values, only the unconstrained joints

    """
    coordSet = model.getCoordinateSet()
    i = 0
    for coord in coordSet:
        if not only_clamped or coord.getClamped(state): # update only not constrained variables
            q[i] = np.clip(q[i], coord.get_range(0),coord.get_range(1))
            coord.setValue(state, q[i], True)
            i = i + 1
    
    model.assemble(state)

def getBodyPosition(model: Model, state: State, name: str):
    """!
    
    Method used to get the positions of the body parts of the model, expressed
    into the global referential Ground.

    Returns:
        D_body_pos : Dictionary of list.
        Dictionary expressing, for each body part, its position and rotation matrix
        into the Ground referential.
    
        Example : 
                D_body_pos[hand_l] = [ M_p, M_R ]
                M_p = np.array([[0.5],
                                [0.5],
                                [0.75]])
                M_R = np.eye(3,3)

    """
    
    bodylist = model.getBodyList()
    R = np.eye(3)
    p = np.zeros(3)
    for body in bodylist:
        if name == body.getName():
            M_R = np.zeros((3,3))
            M_p = np.zeros((3,1))
            p = body.getTransformInGround(state).p().to_numpy()
            R = np.array(body.getTransformInGround(state).R())

    return R, p
    
    # return np.eye(3), np.zeros(3)

def getStationJacobian(model: Model, state: State, body: str or Body, endEffector: list or Vec3, QIndices: list = None):
    """Compute the station Jacobian matrix of the system at the end effector of the given body index"""

    if type(endEffector) is list:
        endEffector = Vec3(endEffector)

    if type(body) is str:
        body = model.getBodySet().get(body)
    bodyEEIndex = body.getMobilizedBodyIndex()

    J = Matrix()
    model.getMatterSubsystem().calcStationJacobian(
        state, bodyEEIndex, endEffector, J)
    J = J.to_numpy()

    if QIndices is not None:
        J = J[:, QIndices]

    return J


def getMuscleTensions(model: Model, state: State, muscleNames: list = None):
    """Compute the minimum and maximum tensions for muscles in muscleNames list.

    Model should be equilibrated before calling this function."""

    if muscleNames is None:
        muscles = model.getMuscles()
    else:
        muscles = [model.getMuscles().get(name) for name in muscleNames]

    t_min = [muscle.getPassiveFiberForce(state) for muscle in muscles]
    #t_min = [0 for muscle in muscles]
    t_max = [muscle.getActiveFiberForce(
        state) + muscle.getPassiveFiberForce(state) for muscle in muscles]

    return t_min, t_max


def getMomentArmMatrix(model: Model, state: State, muscleNames: list = None, coordNames: list = None) -> np.ndarray:
    """Compute and return the moment arm matrix of the model for the given muscles (= columns)
    and the given coordinates (= rows). Order is taken into account.

    Model should be equilibrated before calling this function.

    If no muscles are coordinates are given, computes the whole moment arm matrix in order
    of model.getMuscles() and model.getCoordinateSet()"""

    if coordNames is None:
        coords = model.getCoordinateSet()
    else:
        coords = [model.getCoordinateSet().get(name) for name in coordNames]

    if muscleNames is None:
        muscles = model.getMuscles()
    else:
        muscles = [model.getMuscles().get(name) for name in muscleNames]

    N = np.array([[muscle.computeMomentArm(state, coord)
                  for coord in coords] for muscle in muscles]).T

    N[np.isnan(N)]=0
    return N


def getCoordinatesInMultibodyOrder(model: Model) -> dict:
    """Coordinates in a dict as "coordName": (bodyIndex, QIndexInJoint, object)"""

    model_coordinates = {}  # coordinates in multibody order
    for coord in model.getCoordinateSet():
        mbix = coord.getBodyIndex()
        mqix = coord.getMobilizerQIndex()
        model_coordinates[coord.getName()] = (mbix, mqix, coord)

    model_coordinates = dict(
        sorted(model_coordinates.items(), key=lambda x: x[1]))

    for key, value in model_coordinates.items():
        model_coordinates[key] = value

    return model_coordinates


def getQIndex(model: Model, coord: str or Coordinate) -> int:
    """Returns the index of the coordinate's Q value inside the Q's state vector"""
    if type(coord) is str:
        coord = model.getCoordinateSet().get(coord)

    coordinatesOrdered = getCoordinatesInMultibodyOrder(model)

    qIndex = 0
    for i, value in enumerate(coordinatesOrdered.values()):
        if value[2].getName() == coord.getName():
            qIndex = i
            break
    return qIndex


def getQIndices(model: Model, coordNames: list = None) -> list:
    """Retrieve the indices of the given coordinates name in the Q state vector. 
    Useful to select the Jacobian rows. If no coordNames given, compute for all coordinates."""

    if coordNames is None:
        QIndices = [getQIndex(model, coord)
                    for coord in model.getCoordinateSet()]
    else:
        QIndices = [getQIndex(model, model.getCoordinateSet().get(name))
                    for name in coordNames]
    return QIndices


def getQIndicesOfClampedCoord(model: Model, state: State) -> list:
    """Retrieve the coordinates and indices of clamped coordinated. 
    Useful to select the Jacobian rows"""

    coordNames = []
    for coord in model.getCoordinateSet():
        if coord.getClamped(state):
            coordNames.append(coord.getName())

    QIndices = getQIndices(model, coordNames)
    return  [name for i, name in enumerate(coordNames)], QIndices


def getQIndicesOfAllCoord(model: Model) -> list:
    """Retrieve the coordinates and indices of clamped coordinated. 
    Useful to select the Jacobian rows"""

    coordNames = []
    for coord in model.getCoordinateSet():
        coordNames.append(coord.getName())

    QIndices = getQIndices(model, coordNames)
    return  [name for i, name in enumerate(coordNames)], QIndices