# Opensim examples

If you have already installed the OpenSim with python API then you just clone this repository to your PC. 

> Make sure to clone the submodules as well

```bash
git clone --recurse-submodules https://gitlab.inria.fr/auctus-team/people/antunskuric/example/opensim_examples.git
cd opensim_examples
```

## Opensim using conda

### First build conda package for open-sim:
https://github.com/opensim-org/conda-opensim/tree/use_opensim43

```
git clone https://github.com/opensim-org/conda-opensim.git
cd conda-opensim
conda build opensim
```

### Install anaconda environment 

```
conda env create -f env.yaml
conda activate opensim
```

### Install opensim anaconda package
```
conda install --use-local opensim
```

## Running the examples
Example visualising the models 

<img src="images/vis.png">

```
python test_visaulise.py
```


Examples of polytope calculation

<img src="images/one.png">

```
python test_polytope.py
```

<img src="images/two.png">

```
python test_polytope_bimanual.py
```