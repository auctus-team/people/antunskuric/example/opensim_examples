# include the pyosim module
from utils import getStationJacobian, getMomentArmMatrix, getQIndicesOfClampedCoord, getMuscleTensions, getBodyPosition, setCoordinateValues

# include opensim package
import opensim as osim

# pycappacity for polytope calculationreate the 
from pycapacity.human import force_polytope as polytope

# some utils 
import numpy as np
import time

## Constructor of the OsimModel class.
model = osim.Model('./opensim_models/upper_body/bimanual/MoBL_ARMS_bimanual_6_2_21.osim')

endEffectorBody = 'hand_l'

state  = model.initSystem()
start = time.time()
joint_pos = [0,0,0,0,0,0, # base
            0.0,0.5,0.0,1.3,0.0,1.0,0.0, # left arm
            0.0,1.5,-1.5,1.3,0.0,1.0,0.0] # right arm
setCoordinateValues(model,state,joint_pos)


start = time.time()
coordNames, coordIds = getQIndicesOfClampedCoord(model, state)
model.equilibrateMuscles(state)
J = getStationJacobian(model, state, endEffectorBody, osim.Vec3(0), coordIds[6:])
N = getMomentArmMatrix(model, state, coordNames=coordNames[6:])
F_min, F_max = getMuscleTensions(model, state)
print("time", time.time() - start)

# polytope calculation
start = time.time()
f_vert, H, d, faces_indexes = polytope(J, N, F_min, F_max, 0.1)
print("time", time.time() - start)

#find hand position
hand_orientation, hand_position = getBodyPosition(model,state, endEffectorBody)

# create the polytope
import trimesh
mesh = trimesh.Trimesh(vertices=(f_vert.T/2000 + hand_position.reshape((3,))) ,
                       faces=faces_indexes,  use_embree=True, validate=True)

# save polytope as stl file
f = open("polytope.stl", "wb")
f.write(trimesh.exchange.stl.export_stl(mesh))
f.close()
# adding polytope faces 
mesh = osim.Mesh("./polytope.stl")
model.get_ground().attachGeometry(mesh)
mesh.setColor(osim.Vec3(0.1,0.1,1))
mesh.setOpacity(0.3)
# adding polytope vireframe
mesh = osim.Mesh("./polytope.stl")
model.get_ground().attachGeometry(mesh)
mesh.setColor(osim.Vec3(0.1,0.1,1))
mesh.setRepresentation(2)


# visualise the model and polytope
model.setUseVisualizer(True)
state  = model.initSystem()
mviz = model.getVisualizer()
setCoordinateValues(model, state, joint_pos)
mviz.show(state)


